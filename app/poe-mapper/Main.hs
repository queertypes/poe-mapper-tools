module Main where

import YNotPrelude

-- | query language:
--
-- A query consists of space-delimited specifiers. These specifiers
-- are implicitly AND'd together to realize the final filter.
--
-- The query language proceeds as follows:
--
-- * if no prefix is found, it's assumed to be a map name.
--
-- * for each of the following prefixes:
--   * tier:<int> - show maps that can spawn at that tier
--   * t:<int> - alias for tier:<int>
--   * region:<string> - limit maps to a given region
--   * r:<string> - alias for region:<string>
--     * region can be any of:
--       * haewark_hamlet
--       * tirns_end
--       * lex_proxima
--       * lex_ejoris
--       * new_vastir
--       * glennach_cairns
--       * valdos_rest
--       * lira_arthain
--
--     * for region, can specify just a part of the region name, as
--       long as it's unique. for example, "hae" is enough for
--       "haewark_hamlet", but "lex" isn't enough to distinguish
--       between "lex_proxima" and "lex_ejoris"
--
-- * gems:<int> - show only maps that unlock when <int> gems are socketed
-- * gems_exactly:<int> - alias for gems:<int>
-- * gems_by:<int> - show only maps that unlock by the time <int> gems are socketed
--   * gems_by:2 is equivalent to gems:0 OR gems:1 OR gems:2, for example
--
-- if no filters are found and the query is just a map name, it
-- outputs general information for that map:
-- * map name
-- * map region
-- * map tiers
-- * gems needed to unlock the map
-- * adjacent maps
-- * example: "Volcano, Lex Proxima(0): [T3,T6,T10,T12,T14] {Underground Sea, Coral Ruins, Pit, Thicket, Bone Crypt}"
--
-- if filters are found, then the following info is printed for each
-- map that matches:
-- * map name
-- * map region
-- * map tiers
-- * gems needed to unlock the map
-- * example: "Volcano, Lex Proxima(0): [T3,T6,T10,T12,T14]"

main :: IO ()
main = error "WIP"
