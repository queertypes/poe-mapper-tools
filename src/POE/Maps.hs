module POE.Maps (maps, atlas) where

import POE.Maps.Core
import POE.Maps.Sugar

--------------------------------------------------------------------------------
-- Map Data
--------------------------------------------------------------------------------
maps :: [Map]
maps = [
  leyline
  , vaalPyramid
  , vaultsOfAtziri
  , conservatory
  , academy
  , phantasmagoria
  , chateau
  , perandusManor
  , castleRuins
  , precinct
  , dig
  , malformation
  , shrine
  , grotto
  , terrace
  , wharf
  , strand
  , whakawairuaTuahu
  , alleyways
  , laboratory
  , cursedCrypt
  , cowardsTrial
  , museum
  , putridCloister
  , temple
  , poorjoysAsylum
  , primordialPool
  , waterways
  , tropicalIsland
  , atoll
  , maelstromOfChaos
  , iceberg
  , ghetto
  , citySquare
  , necropolis
  , deathAndTaxes
  , spiderForest
  , jungleValley
  , orchard
  , ashenWood
  , hauntedMansion
  , mausoleum
  , undergroundSea
  , obasCursedTrove
  , fields
  , desert
  , bazaar
  , bog
  , arsenal
  , dunes
  , pillarsOfArun
  , volcano
  , belfry
  , cemetery
  , hallowedGround
  , crystalOre
  , aridLake
  , port
  , maze
  , doryanisMachinarium
  , residence
  , plateau
  , undergroundRiver
  , caerBlaidd
  , ivoryTemple
  , coralRuins
  , arena
  , barrows
  , pit
  , thicket
  , boneCrypt
  , olmecsSanctum
  , caldera
  , reef
  , canyon
  , lair
  , shipyard
  , core
  , crimsonTemple
  , arachnidNest
  , carcass
  , lavaLake
  , arcade
  , cage
  , moonTemple
  , wastePool
  , park
  , courtyard
  , vintakrsSquare
  , basilica
  , twilightTemple
  , primordialBlocks
  , sunkenCity
  , summit
  , acidCaverns
  , beach
  , sulphurVents
  , ramparts
  , glacier
  , armoury
  , fungalHollow
  , pen
  , vault
  , raceCourse
  , coves
  , lavaChambers
  , shore
  , maoKun
  , colonnade
  , plaza
  , marshes
  , excavation
  , cells
  , channel
  , dungeon
  , floodedMine
  , infestedValley
  , spiderLair
  , pier
  , villa
  , peninsula
  , lookout
  , graveyard
  , overgrownRuin
  , mudGeyser
  , mesa
  , wasteland
  , scriptorium
  , burialChambers
  , factory
  , lighthouse
  , overgrownShrine
  , crater
  , actonsNightmare
  , vaalTemple
  , mineralPools
  , promenade
  , hallOfGrandmasters
  , toxicSewer
  , ancientCity
  , relicChambers
  , defiledCathedral
  , arachnidTomb
  , estuary
  , geode
  , tower
  , siege
  , garden
  , sepulchre
  , colosseum
  , darkForest
  , palace
  , desertSpring
  ]

-- NOTE: this adjacency info is only correct for a fully awakened
-- atlas. the combinatronics of all awakened states (notably, border
-- adjacencies don't get shuffled on awakening a region), make it so
-- that it'd take 5 adjacency lists per map per socketing level to
-- fully capture correct adjacency. A good example of this is that in
-- gem:0, Pier is connected to Villa, but in gem:4, Pier is instead
-- connected to Factory and Scriptorium instead of Villa - new maps
-- are inserted between Pier and Villa.
atlas :: Atlas
atlas = mkAtlas [
  --- Haewark Hamlet
  (Leyline, [bazaar, cursedCrypt, conservatory])
  , (VaalPyramid, [conservatory, vaultsOfAtziri, dig, grotto, academy, museum, laboratory])
  , (VaultsOfAtziri, [vaalPyramid])
  , (Conservatory, [leyline, vaalPyramid, dig])
  , (Academy, [vaalPyramid, grotto, castleRuins, temple, museum])
  , (Phantasmagoria, [temple, primordialPool, terrace])
  , (Chateau, [terrace, shrine, colonnade, perandusManor])
  , (PerandusManor, [chateau])
  , (CastleRuins, [academy, grotto, precinct, malformation, shrine, terrace])
  , (Precinct, [malformation, castleRuins, dig])
  , (Dig, [conservatory, grotto, vaalPyramid, precinct])
  , (Malformation, [precinct, castleRuins, shrine])
  , (Shrine, [malformation, castleRuins, chateau])
  , (Grotto, [vaalPyramid, dig, precinct, castleRuins, academy])
  , (Terrace, [phantasmagoria, castleRuins, chateau])

  --- Tirn's End
  , (Wharf, [alleyways, tropicalIsland, atoll, strand])
  , (Strand, [wharf, whakawairuaTuahu, pen, fungalHollow])
  , (WhakawairuaTuahu, [wharf])
  , (Alleyways, [wharf, desert, fields])
  , (Laboratory, [iceberg, cursedCrypt, vaalPyramid, museum])
  , (CursedCrypt, [desert, leyline, cowardsTrial, laboratory, iceberg])
  , (CowardsTrial, [cursedCrypt])
  , (Museum, [laboratory, putridCloister, vaalPyramid, academy, temple, citySquare])
  , (PutridCloister, [museum])
  , (Temple, [museum, academy, phantasmagoria, poorjoysAsylum, spiderForest])
  , (PoorjoysAsylum, [temple])
  , (PrimordialPool, [spiderForest, phantasmagoria, waterways])
  , (Waterways, [primordialPool, necropolis, colonnade])
  , (TropicalIsland, [wharf, citySquare, iceberg])
  , (Atoll, [wharf, citySquare, ghetto, maelstromOfChaos])
  , (MaelstromOfChaos, [atoll])
  , (Iceberg, [tropicalIsland, laboratory, cursedCrypt])
  , (Ghetto, [atoll, spiderForest, necropolis])
  , (CitySquare, [tropicalIsland, atoll, museum, spiderForest])
  , (Necropolis, [ghetto, waterways, deathAndTaxes])
  , (DeathAndTaxes, [necropolis])
  , (SpiderForest, [citySquare, temple, primordialPool, ghetto])

  --- Lex Proxima
  , (JungleValley, [mausoleum, ivoryTemple, aridLake, fields])
  , (Orchard, [lookout, cemetery, mudGeyser])
  , (AshenWood, [cemetery, belfry, residence, hauntedMansion, lookout])
  , (HauntedMansion, [ashenWood, mausoleum, pier, peninsula])
  , (Mausoleum, [residence, undergroundRiver, hauntedMansion, jungleValley])
  , (UndergroundSea, [volcano, dunes, arsenal, maze, aridLake, mausoleum, obasCursedTrove])
  , (ObasCursedTrove, [undergroundSea])
  , (Fields, [ivoryTemple, port, alleyways, jungleValley])
  , (Desert, [port, bazaar, cursedCrypt, alleyways])
  , (Bazaar, [undergroundRiver, leyline, desert])
  , (Bog, [arena, barrows, arsenal, maze])
  , (Arsenal, [pit, undergroundSea, bog])
  , (Dunes, [pit, pillarsOfArun, undergroundSea])
  , (PillarsOfArun, [dunes])
  , (Volcano, [boneCrypt, thicket, pit, undergroundSea, coralRuins])
  , (Belfry, [geode, coralRuins, ashenWood, estuary])
  , (Cemetery, [ashenWood, orchard, mudGeyser, defiledCathedral, relicChambers, hallowedGround])
  , (HallowedGround, [cemetery])
  , (CrystalOre, [coralRuins, canyon, reef])
  , (AridLake, [undergroundSea, maze, ivoryTemple, jungleValley])
  , (Port, [maze, plateau, desert, fields])
  , (Maze, [port, ivoryTemple, aridLake, undergroundSea, arsenal, bog, doryanisMachinarium])
  , (DoryanisMachinarium, [maze])
  , (Residence, [coralRuins, ashenWood, mausoleum])
  , (Plateau, [port, undergroundRiver])
  , (UndergroundRiver, [plateau, bazaar, caerBlaidd])
  , (CaerBlaidd, [undergroundRiver])
  , (IvoryTemple, [aridLake, maze, fields, jungleValley])
  , (CoralRuins, [crystalOre, belfry, volcano, residence])

  --- Lex Ejoris
  , (Arena, [bog, core])
  , (Barrows, [bog, core, arachnidNest])
  , (Pit, [thicket, caldera, arachnidNest, arsenal, dunes, volcano])
  , (Thicket, [boneCrypt, lair, caldera, pit, volcano])
  , (BoneCrypt, [reef, volcano, thicket, olmecsSanctum])
  , (OlmecsSanctum, [boneCrypt])
  , (Caldera, [lair, lavaLake, core, pit, thicket])
  , (Reef, [shipyard, crimsonTemple, lair, boneCrypt, crystalOre])
  , (Canyon, [shipyard, crystalOre, tower])
  , (Lair, [crimsonTemple, carcass, lavaLake, caldera, thicket, reef])
  , (Shipyard, [canyon, reef, crimsonTemple])
  , (Core, [lavaLake, caldera, arachnidNest, barrows, arena])
  , (CrimsonTemple, [carcass, lair, shipyard])
  , (ArachnidNest, [core, barrows, pit])
  , (Carcass, [crimsonTemple, lair, lavaLake])
  , (LavaLake, [carcass, lair, caldera, core])

  --- New Vastir
  , (Arcade, [ramparts, glacier, armoury, moonTemple])
  , (Cage, [raceCourse, coves, moonTemple, park])
  , (MoonTemple, [arcade, raceCourse, cage, wastePool, twilightTemple])
  , (TwilightTemple, [moonTemple])
  , (WastePool, [moonTemple, park, acidCaverns])
  , (Park, [wastePool, cage, coves, courtyard, courthouse, acidCaverns])
  , (Courtyard, [basilica, primordialBlocks, courthouse, park, coves, shore, vintakrsSquare])
  , (VintakrsSquare, [courtyard])
  , (Basilica, [courtyard, shore, plaza, primordialBlocks])
  , (PrimordialBlocks, [basilica, courtyard, courthouse])
  , (SunkenCity, [acidCaverns, summit])
  , (Summit, [courthouse, sunkenCity, primordialBlocks])
  , (AcidCaverns, [wastePool, courthouse, park, sunkenCity])

  --- Glennach Cairns
  , (Beach, [sulphurVents, spiderLair, dungeon, marshes, fungalHollow])
  , (SulphurVents, [villa, ramparts, glacier, beach])
  , (Ramparts, [graveyard, sulphurVents, arcade])
  , (Glacier, [sulphurVents, spiderLair, dungeon, armoury, arcade])
  , (Armoury, [glacier, dungeon, vault, raceCourse, arcade])
  , (FungalHollow, [beach, floodedMine, strand])
  , (Pen, [strand, cells, floodedMine])
  , (Vault, [dungeon, excavation, coves, cage, raceCourse, armoury])
  , (RaceCourse, [armoury, vault, cage, moonTemple])
  , (Coves, [vault, infestedValley, shore, courtyard, park, cage])
  , (LavaChambers, [cells, plaza, colonnade])
  , (Shore, [infestedValley, coves, courtyard, basilica, maoKun])
  , (MaoKun, [shore])
  , (Colonnade, [lavaChambers, waterways, phantasmagoria, chateau])
  , (Plaza, [lavaChambers, channel, basilica])
  , (Marshes, [beach, dungeon, excavation])
  , (Excavation, [marshes, vault, infestedValley, floodedMine])
  , (Cells, [pen, channel, lavaChambers])
  , (Channel, [floodedMine, cells, infestedValley, lavaChambers])
  , (Dungeon, [spiderLair, beach, marshes, vault, armoury, glacier])
  , (FloodedMine, [fungalHollow, pen, channel, excavation])
  , (InfestedValley, [excavation, channel, shore, coves])
  , (SpiderLair, [beach, dungeon, glacier])

  --- Valdo's Rest
  , (Pier, [factory, scriptorium, peninsula, hauntedMansion])
  , (Villa, [factory, burialChambers, graveyard, sulphurVents])
  , (Peninsula, [lookout, pier, mineralPools])
  , (Lookout, [ashenWood, orchard, crater, peninsula])
  , (Graveyard, [villa, ramparts, lighthouse, overgrownRuin, overgrownShrine, wasteland])
  , (OvergrownRuin, [graveyard, lighthouse, overgrownShrine])
  , (MudGeyser, [defiledCathedral, cemetery, orchard, crater, mesa])
  , (Mesa, [toxicSewer, ancientCity, mudGeyser, burialChambers, wasteland])
  , (Wasteland, [toxicSewer, mesa, burialChambers, graveyard])
  , (Scriptorium, [mineralPools, pier])
  , (BurialChambers, [mudGeyser, wasteland, mineralPools, crater, villa])
  , (Factory, [pier, villa])
  , (Lighthouse, [overgrownRuin, graveyard])
  , (OvergrownShrine, [overgrownRuin, promenade, graveyard, actonsNightmare])
  , (ActonsNightmare, [overgrownShrine])
  , (Crater, [lookout, burialChambers, mudGeyser])
  , (VaalTemple, [])
  , (MineralPools, [scriptorium, peninsula, burialChambers])
  , (Promenade, [overgrownShrine, hallOfGrandmasters])
  , (HallOfGrandmasters, [promenade])

  --- Lira Arthain
  , (ToxicSewer, [sepulchre, ancientCity, mesa, wasteland])
  , (AncientCity, [toxicSewer, mesa, sepulchre, defiledCathedral])
  , (RelicChambers, [cemetery, estuary, defiledCathedral])
  , (DefiledCathedral, [relicChambers, cemetery, mudGeyser, ancientCity, sepulchre, desertSpring, arachnidTomb])
  , (ArachnidTomb, [siege, defiledCathedral, desertSpring])
  , (Estuary, [relicChambers, belfry, siege, geode])
  , (Geode, [belfry, siege, estuary, tower])
  , (Tower, [canyon, geode, siege, colosseum])
  , (Siege, [darkForest, arachnidTomb, palace, estuary, geode, tower, colosseum])
  , (Garden, [darkForest, sepulchre, desertSpring])
  , (Sepulchre, [garden, defiledCathedral, toxicSewer])
  , (Colosseum, [darkForest, siege, tower])
  , (DarkForest, [palace, garden, siege, colosseum])
  , (Palace, [siege, darkForest])
  , (DesertSpring, [garden, defiledCathedral, arachnidTomb])
  ]
