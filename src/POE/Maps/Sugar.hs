module POE.Maps.Sugar (
  -- * Tiering Sugar
    t1
  , t2
  , t3
  , t4
  , t5
  , t6
  , t7
  , t8
  , t9
  , t10
  , t11
  , t12
  , t13
  , t14
  , t15
  , t16

  -- * Region Sugar
  , hamlet
  , tirn
  , lexP
  , lexE
  , vastir
  , cairns
  , valdos
  , lira

  -- * Map Sugar
  -- ** Haewark Hamlet
  , leyline
  , vaalPyramid
  , vaultsOfAtziri
  , conservatory
  , academy
  , phantasmagoria
  , chateau
  , perandusManor
  , castleRuins
  , precinct
  , dig
  , malformation
  , shrine
  , grotto
  , terrace

  -- ** Tirn's End
  , wharf
  , strand
  , whakawairuaTuahu
  , alleyways
  , laboratory
  , cursedCrypt
  , cowardsTrial
  , museum
  , putridCloister
  , temple
  , poorjoysAsylum
  , primordialPool
  , waterways
  , tropicalIsland
  , atoll
  , maelstromOfChaos
  , iceberg
  , ghetto
  , citySquare
  , necropolis
  , deathAndTaxes
  , spiderForest

  -- ** Lex Proxima
  , jungleValley
  , orchard
  , ashenWood
  , hauntedMansion
  , mausoleum
  , undergroundSea
  , obasCursedTrove
  , fields
  , desert
  , bazaar
  , bog
  , arsenal
  , dunes
  , pillarsOfArun
  , volcano
  , belfry
  , cemetery
  , hallowedGround
  , crystalOre
  , aridLake
  , port
  , maze
  , doryanisMachinarium
  , residence
  , plateau
  , undergroundRiver
  , caerBlaidd
  , ivoryTemple
  , coralRuins

  -- ** Lex Ejoris
  , arena
  , barrows
  , pit
  , thicket
  , boneCrypt
  , olmecsSanctum
  , caldera
  , reef
  , canyon
  , lair
  , shipyard
  , core
  , crimsonTemple
  , arachnidNest
  , carcass
  , lavaLake

  -- ** New Vastir
  , arcade
  , cage
  , moonTemple
  , twilightTemple
  , wastePool
  , park
  , courtyard
  , vintakrsSquare
  , basilica
  , courthouse
  , primordialBlocks
  , sunkenCity
  , summit
  , acidCaverns

  -- ** Glennach Cairns
  , beach
  , sulphurVents
  , ramparts
  , glacier
  , armoury
  , fungalHollow
  , pen
  , vault
  , raceCourse
  , coves
  , lavaChambers
  , shore
  , maoKun
  , colonnade
  , plaza
  , marshes
  , excavation
  , cells
  , floodedMine
  , channel
  , dungeon
  , infestedValley
  , spiderLair

  -- ** Valdo's Rest
  , pier
  , villa
  , peninsula
  , lookout
  , graveyard
  , overgrownRuin
  , mudGeyser
  , mesa
  , wasteland
  , scriptorium
  , burialChambers
  , factory
  , lighthouse
  , overgrownShrine
  , crater
  , actonsNightmare
  , vaalTemple
  , mineralPools
  , promenade
  , hallOfGrandmasters

  -- ** Lira Arthain
  , toxicSewer
  , ancientCity
  , relicChambers
  , defiledCathedral
  , arachnidTomb
  , estuary
  , geode
  , tower
  , siege
  , garden
  , sepulchre
  , colosseum
  , darkForest
  , palace
  , desertSpring
) where

import POE.Maps.Core

---- Tiering Sugar
t1 :: Tier
t1 = T1

t2 :: Tier
t2 = T2

t3 :: Tier
t3 = T3

t4 :: Tier
t4 = T4

t5 :: Tier
t5 = T5

t6 :: Tier
t6 = T6

t7 :: Tier
t7 = T7

t8 :: Tier
t8 = T8

t9 :: Tier
t9 = T9

t10 :: Tier
t10 = T10

t11 :: Tier
t11 = T11

t12 :: Tier
t12 = T12

t13 :: Tier
t13 = T13

t14 :: Tier
t14 = T14

t15 :: Tier
t15 = T15

t16 :: Tier
t16 = T16

---- Region Sugar
hamlet :: Region
hamlet = HaewarkHamlet

tirn :: Region
tirn = TirnsEnd

lexP :: Region
lexP = LexProxima

lexE :: Region
lexE = LexEjoris

vastir :: Region
vastir = NewVastir

cairns :: Region
cairns = GlennachCairns

valdos :: Region
valdos = ValdosRest

lira :: Region
lira = LiraArthain

---- Map Sugar
--- Haewark Hamlet
leyline :: Map
leyline = Map Leyline [t3, t6, t10, t12, t14] hamlet

vaalPyramid :: Map
vaalPyramid = Map VaalPyramid [t3, t7, t10, t12, t14] hamlet

vaultsOfAtziri :: Map
vaultsOfAtziri = Map VaultsOfAtziri [t3, t7, t10, t12, t14] hamlet

conservatory :: Map
conservatory = Map Conservatory [t4, t7, t10, t12, t14] hamlet

academy :: Map
academy = Map Academy [t4, t7, t11, t12, t14] hamlet

phantasmagoria :: Map
phantasmagoria = Map Phantasmagoria [t4, t7, t11, t13, t15] hamlet

chateau :: Map
chateau = Map Chateau [t5, t9, t12, t14, t16] hamlet

perandusManor :: Map
perandusManor = Map PerandusManor [t5, t9, t12, t14, t16] hamlet

castleRuins :: Map
castleRuins = Map CastleRuins [t5, t8, t12, t13, t15] hamlet

precinct :: Map
precinct = Map Precinct [t7, t12, t14, t15] hamlet

dig :: Map
dig = Map Dig [t11, t13, t15] hamlet

malformation :: Map
malformation = Map Malformation [t14, t16] hamlet

shrine :: Map
shrine = Map Shrine [t14, t16] hamlet

grotto :: Map
grotto = Map Grotto [t15] hamlet

terrace :: Map
terrace = Map Terrace [t15] hamlet

--- Tirn's End
wharf :: Map
wharf = Map Wharf [t1, t4, t8, t10, t14] tirn

strand :: Map
strand = Map Strand [t2, t5, t9, t11, t14] tirn

whakawairuaTuahu :: Map
whakawairuaTuahu = Map WhakawairuaTuahu [t2, t5, t9, t11, t14] tirn

alleyways :: Map
alleyways = Map Alleyways [t2, t5, t9, t11, t14] tirn

laboratory :: Map
laboratory = Map Laboratory [t2, t6, t9, t11, t14] tirn

cursedCrypt :: Map
cursedCrypt = Map CursedCrypt [t2, t6, t9, t11, t14] tirn

cowardsTrial :: Map
cowardsTrial = Map CowardsTrial [t2, t6, t9, t11, t14] tirn

museum :: Map
museum = Map Museum [t3, t6, t10, t12, t14] tirn

putridCloister :: Map
putridCloister = Map PutridCloister [t3, t6, t10, t12, t14] tirn

temple :: Map
temple = Map Temple [t3, t6, t10, t12, t14] tirn

poorjoysAsylum :: Map
poorjoysAsylum = Map PoorjoysAsylum [t3, t6, t10, t12, t14] tirn

primordialPool :: Map
primordialPool = Map PrimordialPool [t3, t6, t10, t12, t15] tirn

waterways :: Map
waterways = Map Waterways [t3, t7, t10, t13, t16] tirn

tropicalIsland :: Map
tropicalIsland = Map TropicalIsland [t5, t9, t11, t14] tirn

atoll :: Map
atoll = Map Atoll [t5, t9, t11, t14] tirn

maelstromOfChaos :: Map
maelstromOfChaos = Map MaelstromOfChaos [t5, t9, t11, t14] tirn

iceberg :: Map
iceberg = Map Iceberg [t9, t11, t14] tirn

ghetto :: Map
ghetto = Map Ghetto [t12, t14] tirn

citySquare :: Map
citySquare = Map CitySquare [t11, t14] tirn

necropolis :: Map
necropolis = Map Necropolis [t15] tirn

deathAndTaxes :: Map
deathAndTaxes = Map DeathAndTaxes [t15] tirn

spiderForest :: Map
spiderForest = Map SpiderForest [t14] tirn

--- Lex Proxima
jungleValley :: Map
jungleValley = Map JungleValley [t1, t4, t8, t10, t14] lexP

orchard :: Map
orchard = Map Orchard [t2, t6, t9, t11, t14] lexP

ashenWood :: Map
ashenWood = Map AshenWood [t2, t6, t9, t11, t14] lexP

hauntedMansion :: Map
hauntedMansion = Map HauntedMansion [t2, t5, t9, t11, t14] lexP

mausoleum :: Map
mausoleum = Map Mausoleum [t2, t5, t9, t11, t14] lexP

undergroundSea :: Map
undergroundSea = Map UndergroundSea [t2, t6, t9, t11, t14] lexP

obasCursedTrove :: Map
obasCursedTrove = Map ObasCursedTrove [t2, t6, t9, t11, t14] lexP

fields :: Map
fields = Map Fields [t2, t5, t9, t11, t14] lexP

desert :: Map
desert = Map Desert [t2, t6, t9, t11, t14] lexP

bazaar :: Map
bazaar = Map Bazaar [t3, t6, t10, t12, t15] lexP

bog :: Map
bog = Map Bog [t3, t6, t10, t12, t14] lexP

arsenal :: Map
arsenal = Map Arsenal [t3, t7, t10, t12, t14] lexP

dunes :: Map
dunes = Map Dunes [t3, t7, t10, t12, t14] lexP

pillarsOfArun :: Map
pillarsOfArun = Map PillarsOfArun [t3, t7, t10, t12, t14] lexP

volcano :: Map
volcano = Map Volcano [t3, t6, t10, t12, t14] lexP

belfry :: Map
belfry = Map Belfry [t3, t7, t10, t12, t14] lexP

cemetery :: Map
cemetery = Map Cemetery [t3, t7, t10, t11, t14] lexP

hallowedGround :: Map
hallowedGround = Map HallowedGround [t3, t7, t10, t11, t14] lexP

crystalOre :: Map
crystalOre = Map CrystalOre [t4, t7, t11, t13, t16] lexP

aridLake :: Map
aridLake = Map AridLake [t5, t9, t11, t14] lexP

port :: Map
port = Map Port [t6, t10, t12, t15] lexP

maze :: Map
maze = Map Maze [t9, t11, t14] lexP

doryanisMachinarium :: Map
doryanisMachinarium = Map DoryanisMachinarium [t9, t11, t14] lexP

residence :: Map
residence = Map Residence [t10, t12, t14] lexP

plateau :: Map
plateau = Map Plateau [t13, t16] lexP

undergroundRiver :: Map
undergroundRiver = Map UndergroundRiver [t13, t16] lexP

caerBlaidd :: Map
caerBlaidd = Map CaerBlaidd [t13, t16] lexP

ivoryTemple :: Map
ivoryTemple = Map IvoryTemple [t14] lexP

coralRuins :: Map
coralRuins = Map CoralRuins [t15] lexP

--- Lex Ejoris
arena :: Map
arena = Map Arena [t3, t6, t10, t12, t14] lexE

barrows :: Map
barrows = Map Barrows [t4, t7, t10, t12, t14] lexE

pit :: Map
pit = Map Pit [t4, t7, t10, t12, t14] lexE

thicket :: Map
thicket = Map Thicket [t4, t7, t10, t12, t15] lexE

boneCrypt :: Map
boneCrypt = Map BoneCrypt [t4, t7, t11, t12, t15] lexE

olmecsSanctum :: Map
olmecsSanctum = Map OlmecsSanctum [t4, t7, t11, t12, t15] lexE

caldera :: Map
caldera = Map Caldera [t5, t8, t11, t12, t15] lexE

reef :: Map
reef = Map Reef [t5, t8, t11, t13, t15] lexE

canyon :: Map
canyon = Map Canyon [t5, t8, t12, t14, t16] lexE

lair :: Map
lair = Map Lair [t8, t12, t13, t15] lexE

shipyard :: Map
shipyard = Map Shipyard [t9, t12, t14, t16] lexE

core :: Map
core = Map Core [t11, t13, t15] lexE

crimsonTemple :: Map
crimsonTemple = Map CrimsonTemple [t14, t16] lexE

arachnidNest :: Map
arachnidNest = Map ArachnidNest [t12, t14] lexE

carcass :: Map
carcass = Map Carcass [t16] lexE

lavaLake :: Map
lavaLake = Map LavaLake [t15] lexE

--- New Vastir
arcade :: Map
arcade = Map Arcade [t3, t6, t10, t11, t14] vastir

cage :: Map
cage = Map Cage [t3, t7, t10, t12, t14] vastir

moonTemple :: Map
moonTemple = Map MoonTemple [t3, t7, t10, t12, t14] vastir

twilightTemple :: Map
twilightTemple = Map TwilightTemple [t3, t7, t10, t12, t14] vastir

wastePool :: Map
wastePool = Map WastePool [t3, t8, t11, t12, t15] vastir

park :: Map
park = Map Park [t4, t8, t11, t12, t15] vastir

courtyard :: Map
courtyard = Map Courtyard [t4, t8, t11, t13, t15] vastir

vintakrsSquare :: Map
vintakrsSquare = Map VintakrsSquare [t4, t8, t11, t13, t15] vastir

basilica :: Map
basilica = Map Basilica [t5, t9, t12, t14, t16] vastir

courthouse :: Map
courthouse = Map Courthouse [t9, t12, t13, t15] vastir

primordialBlocks :: Map
primordialBlocks = Map PrimordialBlocks [t12, t14, t16] vastir

sunkenCity :: Map
sunkenCity = Map SunkenCity [t14, t16] vastir

summit :: Map
summit = Map Summit [t16] vastir

acidCaverns :: Map
acidCaverns = Map AcidCaverns [t15] vastir

--- Glennach Cairns
beach :: Map
beach = Map Beach [t1, t4, t8, t10, t14] cairns

sulphurVents :: Map
sulphurVents = Map SulphurVents [t2, t5, t9, t11, t14] cairns

ramparts :: Map
ramparts = Map Ramparts [t2, t6, t9, t11, t14] cairns

glacier :: Map
glacier = Map Glacier [t2, t6, t9, t11, t14] cairns

armoury :: Map
armoury = Map Armoury [t2, t5, t9, t11, t14] cairns

fungalHollow :: Map
fungalHollow = Map FungalHollow [t2, t5, t9, t11, t14] cairns

pen :: Map
pen = Map Pen [t3, t6, t9, t12, t14] cairns

vault :: Map
vault = Map Vault [t3, t6, t10, t11, t14] cairns

raceCourse :: Map
raceCourse = Map RaceCourse [t3, t6, t10, t11, t14] cairns

coves :: Map
coves = Map Coves [t3, t7, t10, t12, t15] cairns

lavaChambers :: Map
lavaChambers = Map LavaChambers [t4, t7, t11, t14, t16] cairns

shore :: Map
shore = Map Shore [t4, t7, t11, t13, t15] cairns

maoKun :: Map
maoKun = Map MaoKun [t4, t7, t11, t13, t15] cairns

colonnade :: Map
colonnade = Map Colonnade [t4, t8, t11, t13, t16] cairns

plaza :: Map
plaza = Map Plaza [t4, t8, t12, t14, t16] cairns

marshes :: Map
marshes = Map Marshes [t5, t9, t11, t14] cairns

excavation :: Map
excavation = Map Excavation [t6, t10, t12, t14] cairns

cells :: Map
cells = Map Cells [t10, t13, t15] cairns

floodedMine :: Map
floodedMine = Map FloodedMine [t9, t12, t14] cairns

channel :: Map
channel = Map Channel [t13, t15] cairns

dungeon :: Map
dungeon = Map Dungeon [t11, t14] cairns

infestedValley :: Map
infestedValley = Map InfestedValley [t14] cairns

spiderLair :: Map
spiderLair = Map SpiderLair [t14] cairns

--- Valdo's Rest
pier :: Map
pier = Map Pier [t1, t4, t8, t10, t14] valdos

villa :: Map
villa = Map Villa [t2, t5, t9, t11, t14] valdos

peninsula :: Map
peninsula = Map Peninsula [t2, t5, t9, t11, t14] valdos

lookout :: Map
lookout = Map Lookout [t2, t6, t9, t11, t14] valdos

graveyard :: Map
graveyard = Map Graveyard [t2, t6, t9, t11, t14] valdos

overgrownRuin :: Map
overgrownRuin = Map OvergrownRuin [t3, t7, t10, t12, t15] valdos

mudGeyser :: Map
mudGeyser = Map MudGeyser [t3, t6, t10, t11, t14] valdos

mesa :: Map
mesa = Map Mesa [t3, t6, t10, t11, t14] valdos

wasteland :: Map
wasteland = Map Wasteland [t3, t6, t10, t11, t14] valdos

scriptorium :: Map
scriptorium = Map Scriptorium [t5, t9, t11, t14] valdos

burialChambers :: Map
burialChambers = Map BurialChambers [t6, t9, t11, t14] valdos

factory :: Map
factory = Map Factory [t9, t11, t15] valdos

lighthouse :: Map
lighthouse = Map Lighthouse [t10, t12, t14] valdos

overgrownShrine :: Map
overgrownShrine = Map OvergrownShrine [t13, t16] valdos

crater :: Map
crater = Map Crater [t11, t14] valdos

actonsNightmare :: Map
actonsNightmare = Map ActonsNightmare [t13, t16] valdos

vaalTemple :: Map
vaalTemple = Map VaalTemple [t16] valdos

mineralPools :: Map
mineralPools = Map MineralPools [t14] valdos

promenade :: Map
promenade = Map Promenade [t16] valdos

hallOfGrandmasters :: Map
hallOfGrandmasters = Map HallOfGrandmasters [t16] valdos

--- Lira Arthain
toxicSewer :: Map
toxicSewer = Map ToxicSewer [t3, t7, t10, t12, t15] lira

ancientCity :: Map
ancientCity = Map AncientCity [t3, t6, t10, t12, t14] lira

relicChambers :: Map
relicChambers = Map RelicChambers [t3, t7, t10, t11, t14] lira

defiledCathedral :: Map
defiledCathedral = Map DefiledCathedral [t4, t7, t10, t12, t14] lira

arachnidTomb :: Map
arachnidTomb = Map ArachnidTomb [t4, t8, t11, t13, t15] lira

estuary :: Map
estuary = Map Estuary [t4, t7, t10, t12, t15] lira

geode :: Map
geode = Map Geode [t4, t8, t11, t13, t15] lira

tower :: Map
tower = Map Tower [t5, t9, t12, t14, t16] lira

siege :: Map
siege = Map Siege [t8, t11, t13, t15] lira

garden :: Map
garden = Map Garden [t9, t12, t13, t16] lira

sepulchre :: Map
sepulchre = Map Sepulchre [t11, t13, t15] lira

colosseum :: Map
colosseum = Map Colosseum [t12, t14, t16] lira

darkForest :: Map
darkForest = Map DarkForest [t14, t16] lira

palace :: Map
palace = Map Palace [t15] lira

desertSpring :: Map
desertSpring = Map DesertSpring [t15] lira
