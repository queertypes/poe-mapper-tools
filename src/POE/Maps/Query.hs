module POE.Maps.Query (
  -- * Filter/Query Types
  MapQuery(..)
  , FilterExpr(..)

  -- * Regional Filters
  , inHaewarkHamlet
  , inTirnsEnd
  , inLexProxima
  , inLexEjoris
  , inNewVastir
  , inGlennachCairns
  , inValdosRest
  , inLiraArthain

  -- * Map Tier Filters
  , t1s
  , t2s
  , t3s
  , t4s
  , t5s
  , t6s
  , t7s
  , t8s
  , t9s
  , t10s
  , t11s
  , t12s
  , t13s
  , t14s
  , t15s
  , t16s

  -- * Region Gem Level Filters
  , unlockedAtStart
  , unlockedAt1Gem
  , unlockedAt2Gems
  , unlockedAt3Gems
  , unlockedAt4Gems
  , unlockedWithin1Gem
  , unlockedWithin2Gems
  , unlockedWithin3Gems
  , unlockedWithin4Gems

  -- * Structural Filters
  , connectedMaps

  -- * Drop Filters
  , canDropFrom
) where

import YNotPrelude hiding (Map)

import Data.List (nub, sort)
import qualified Data.Map as M

import POE.Maps.Core
import POE.Maps.Sugar

data MapQuery
  = MapLookup Name -- ^ General information about a map. e.g., "volcano"
  | MapFilters FilterExpr -- ^ Summary info about maps matching some criteria.
                          --   e.g., "region:hae tier:5"
  | AdjacencyCheck Name -- ^ Summary info about maps next to given map.
                        --   e.g., "next_to:volcano"
  | MapDrops Name Tier -- ^ Potential drops from running a map at a given tier.
                       --   e.g., "drops_from:volcano,t6"

data FilterExpr
  = RegionFilter Region FilterExpr
  | TierFilter Tier FilterExpr
  | UnlockAtGemLevel Int FilterExpr
  | UnlockByGemLevel Int FilterExpr
  | FilterDone

data QueryError
  = QueryFilterError FilterError -- ^ Filter specification errors
  | UnknownQuery Text -- ^ if an unknown query specifier is given, e.g. "freshness:x"
  | InvalidQuery Text -- ^ if it's impossible to tell which query type was meant
  | NonIntegralTier Text
  | OutofBoundsTier Int
  | UnknownMap Text

data FilterError
  = UnknownRegion Text
  | NonIntegralGemLevel Text
  | OutOfBoundsGemLevel Int
  | RedundantGemLevelFilter -- ^ Occurs if more than one unlock_* filter is given

-- region filters
inHaewarkHamlet :: [Map] -> [Map]
inHaewarkHamlet = filter (\(Map _ _ r) -> r == hamlet)

inTirnsEnd :: [Map] -> [Map]
inTirnsEnd = filter (\(Map _ _ r) -> r == tirn)

inLexProxima :: [Map] -> [Map]
inLexProxima = filter (\(Map _ _ r) -> r == lexP)

inLexEjoris :: [Map] -> [Map]
inLexEjoris = filter (\(Map _ _ r) -> r == lexE)

inNewVastir :: [Map] -> [Map]
inNewVastir = filter (\(Map _ _ r) -> r == vastir)

inGlennachCairns :: [Map] -> [Map]
inGlennachCairns = filter (\(Map _ _ r) -> r == cairns)

inValdosRest :: [Map] -> [Map]
inValdosRest = filter (\(Map _ _ r) -> r == valdos)

inLiraArthain :: [Map] -> [Map]
inLiraArthain = filter (\(Map _ _ r) -> r == lira)

-- tier filters
t1s :: [Map] -> [Map]
t1s = filter (\(Map _ ts _) -> t1 `elem` ts)

t2s :: [Map] -> [Map]
t2s = filter (\(Map _ ts _) -> t2 `elem` ts)

t3s :: [Map] -> [Map]
t3s = filter (\(Map _ ts _) -> t3 `elem` ts)

t4s :: [Map] -> [Map]
t4s = filter (\(Map _ ts _) -> t4 `elem` ts)

t5s :: [Map] -> [Map]
t5s = filter (\(Map _ ts _) -> t5 `elem` ts)

t6s :: [Map] -> [Map]
t6s = filter (\(Map _ ts _) -> t6 `elem` ts)

t7s :: [Map] -> [Map]
t7s = filter (\(Map _ ts _) -> t7 `elem` ts)

t8s :: [Map] -> [Map]
t8s = filter (\(Map _ ts _) -> t8 `elem` ts)

t9s :: [Map] -> [Map]
t9s = filter (\(Map _ ts _) -> t9 `elem` ts)

t10s :: [Map] -> [Map]
t10s = filter (\(Map _ ts _) -> t10 `elem` ts)

t11s :: [Map] -> [Map]
t11s = filter (\(Map _ ts _) -> t11 `elem` ts)

t12s :: [Map] -> [Map]
t12s = filter (\(Map _ ts _) -> t12 `elem` ts)

t13s :: [Map] -> [Map]
t13s = filter (\(Map _ ts _) -> t13 `elem` ts)

t14s :: [Map] -> [Map]
t14s = filter (\(Map _ ts _) -> t14 `elem` ts)

t15s :: [Map] -> [Map]
t15s = filter (\(Map _ ts _) -> t15 `elem` ts)

t16s :: [Map] -> [Map]
t16s = filter (\(Map _ ts _) -> t16 `elem` ts)

-- region gem level filters
unlockedAtStart :: [Map] -> [Map]
unlockedAtStart = filter (\m -> gemRequirement m == 0)

unlockedAt1Gem :: [Map] -> [Map]
unlockedAt1Gem = filter (\m -> gemRequirement m == 1)

unlockedAt2Gems :: [Map] -> [Map]
unlockedAt2Gems = filter (\m -> gemRequirement m == 2)

unlockedAt3Gems :: [Map] -> [Map]
unlockedAt3Gems = filter (\m -> gemRequirement m == 3)

unlockedAt4Gems :: [Map] -> [Map]
unlockedAt4Gems = filter (\m -> gemRequirement m == 4)

unlockedWithin1Gem :: [Map] -> [Map]
unlockedWithin1Gem = filter (\m -> gemRequirement m <= 1)

unlockedWithin2Gems :: [Map] -> [Map]
unlockedWithin2Gems = filter (\m -> gemRequirement m <= 2)

unlockedWithin3Gems :: [Map] -> [Map]
unlockedWithin3Gems = filter (\m -> gemRequirement m <= 3)

unlockedWithin4Gems :: [Map] -> [Map]
unlockedWithin4Gems = filter (\m -> gemRequirement m <= 4)

-- structural filters
connectedMaps :: Name -> Atlas -> [Map]
connectedMaps n atlas = maybe [] identity (M.lookup n atlas)

-- map drop rules - a map can drop if:
-- * (it is connected to the current map
-- * or: it has been completed before and is within tier+2 of the current map)
-- * and: gem/unlock seems to affect this but it's unclear how
canDropFrom :: Name -> Tier -> Atlas -> CompletedMaps -> [Map]
canDropFrom n tier atlas (CompletedMaps completed) =
  let maxTier = nextTier (nextTier tier) -- boss drops a map
      adjacentDrops = connectedMaps n atlas
      completionDrops = dropFromCompletion maxTier completed
  in (nub . sort) (adjacentDrops <> completionDrops)

--------------------------------------------------------------------------------
-- Auxilliary Functions
--------------------------------------------------------------------------------
gemRequirement :: Map -> Word
gemRequirement (Map _ ts _)
  | tsCnt == 5 = 0
  | tsCnt == 4 = 1
  | tsCnt == 3 = 2
  | tsCnt == 2 = 3
  | tsCnt == 1 = 4
  | otherwise = 5 -- impossible; a malformed map
  where tsCnt = length ts

dropFromCompletion :: Tier -> [Map] -> [Map]
dropFromCompletion maxTier =
  foldl' step []
  where step acc m@(Map _ ts _) =
          if any (<= maxTier) ts
          then m:acc
          else acc
