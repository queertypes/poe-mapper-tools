module POE.Maps.Parse (
  nameFromText,
  mapFromName,
  mapNames
) where

import YNotPrelude hiding (Map)

import qualified Data.Text as T

import POE.Maps.Core
import POE.Maps.Sugar


-- TODO: migrate this parser to a RoseTree so that minimum unique
-- prefix matches can be supported, like "vol" -> Volcano and "vin" ->
-- Vinktar's Square.
nameFromText :: Text -> Maybe Name
nameFromText t = case T.toLower t of
  "academy" -> Just Academy
  "acidcaverns" -> Just AcidCaverns
  "actonsnightmare" -> Just ActonsNightmare
  "alleyways" -> Just Alleyways
  "ancientcity" -> Just AncientCity
  "arachnidnest" -> Just ArachnidNest
  "arachnidtomb" -> Just ArachnidTomb
  "arcade" -> Just Arcade
  "arena" -> Just Arena
  "aridlake" -> Just AridLake
  "armoury" -> Just Armoury
  "arsenal" -> Just Arsenal
  "ashenwood" -> Just AshenWood
  "atoll" -> Just Atoll
  "barrows" -> Just Barrows
  "basilica" -> Just Basilica
  "bazaar" -> Just Bazaar
  "beach" -> Just Beach
  "belfry" -> Just Belfry
  "bog" -> Just Bog
  "bonecrypt" -> Just BoneCrypt
  "burialchambers" -> Just BurialChambers
  "caerblaidd" -> Just CaerBlaidd
  "cage" -> Just Cage
  "caldera" -> Just Caldera
  "canyon" -> Just Canyon
  "carcass" -> Just Carcass
  "castleruins" -> Just CastleRuins
  "cells" -> Just Cells
  "cemetery" -> Just Cemetery
  "channel" -> Just Channel
  "chateau" -> Just Chateau
  "citysquare" -> Just CitySquare
  "colonnade" -> Just Colonnade
  "colosseum" -> Just Colosseum
  "conservatory" -> Just Conservatory
  "coralruins" -> Just CoralRuins
  "core" -> Just Core
  "courthouse" -> Just Courthouse
  "courtyard" -> Just Courtyard
  "coves" -> Just Coves
  "cowardstrial" -> Just CowardsTrial
  "crater" -> Just Crater
  "crimsontemple" -> Just CrimsonTemple
  "crystalore" -> Just CrystalOre
  "cursedcrypt" -> Just CursedCrypt
  "darkforest" -> Just DarkForest
  "deathandtaxes" -> Just DeathAndTaxes
  "defiledcathedral" -> Just DefiledCathedral
  "desert" -> Just Desert
  "desertspring" -> Just DesertSpring
  "dig" -> Just Dig
  "doryanismachinarium" -> Just DoryanisMachinarium
  "dunes" -> Just Dunes
  "dungeon" -> Just Dungeon
  "estuary" -> Just Estuary
  "excavation" -> Just Excavation
  "factory" -> Just Factory
  "fields" -> Just Fields
  "floodedmine" -> Just FloodedMine
  "fungalhollow" -> Just FungalHollow
  "garden" -> Just Garden
  "geode" -> Just Geode
  "ghetto" -> Just Ghetto
  "glacier" -> Just Glacier
  "graveyard" -> Just Graveyard
  "grotto" -> Just Grotto
  "hallofgrandmasters" -> Just HallOfGrandmasters
  "hallowedground" -> Just HallowedGround
  "hauntedmansion" -> Just HauntedMansion
  "iceberg" -> Just Iceberg
  "infestedvalley" -> Just InfestedValley
  "ivorytemple" -> Just IvoryTemple
  "junglevalley" -> Just JungleValley
  "laboratory" -> Just Laboratory
  "lair" -> Just Lair
  "lavachambers" -> Just LavaChambers
  "lavalake" -> Just LavaLake
  "leyline" -> Just Leyline
  "lighthouse" -> Just Lighthouse
  "lookout" -> Just Lookout
  "maelstromofchaos" -> Just MaelstromOfChaos
  "malformation" -> Just Malformation
  "maokun" -> Just MaoKun
  "marshes" -> Just Marshes
  "mausoleum" -> Just Mausoleum
  "maze" -> Just Maze
  "mesa" -> Just Mesa
  "mineralpools" -> Just MineralPools
  "moontemple" -> Just MoonTemple
  "mudgeyser" -> Just MudGeyser
  "museum" -> Just Museum
  "necropolis" -> Just Necropolis
  "obascursedtrove" -> Just ObasCursedTrove
  "olmecssanctum" -> Just OlmecsSanctum
  "orchard" -> Just Orchard
  "overgrownruin" -> Just OvergrownRuin
  "overgrownshrine" -> Just OvergrownShrine
  "palace" -> Just Palace
  "park" -> Just Park
  "pen" -> Just Pen
  "peninsula" -> Just Peninsula
  "perandusmanor" -> Just PerandusManor
  "phantasmagoria" -> Just Phantasmagoria
  "pier" -> Just Pier
  "pillarsofarun" -> Just PillarsOfArun
  "pit" -> Just Pit
  "plateau" -> Just Plateau
  "plaza" -> Just Plaza
  "poorjoysasylum" -> Just PoorjoysAsylum
  "port" -> Just Port
  "precinct" -> Just Precinct
  "primordialblocks" -> Just PrimordialBlocks
  "primordialpool" -> Just PrimordialPool
  "promenade" -> Just Promenade
  "putridcloister" -> Just PutridCloister
  "racecourse" -> Just RaceCourse
  "ramparts" -> Just Ramparts
  "reef" -> Just Reef
  "relicchambers" -> Just RelicChambers
  "residence" -> Just Residence
  "scriptorium" -> Just Scriptorium
  "sepulchre" -> Just Sepulchre
  "shipyard" -> Just Shipyard
  "shore" -> Just Shore
  "shrine" -> Just Shrine
  "siege" -> Just Siege
  "spiderforest" -> Just SpiderForest
  "spiderlair" -> Just SpiderLair
  "strand" -> Just Strand
  "sulphurvents" -> Just SulphurVents
  "summit" -> Just Summit
  "sunkencity" -> Just SunkenCity
  "temple" -> Just Temple
  "terrace" -> Just Terrace
  "thicket" -> Just Thicket
  "tower" -> Just Tower
  "toxicsewer" -> Just ToxicSewer
  "tropicalisland" -> Just TropicalIsland
  "twilighttemple" -> Just TwilightTemple
  "undergroundriver" -> Just UndergroundRiver
  "undergroundsea" -> Just UndergroundSea
  "vaalpyramid" -> Just VaalPyramid
  "vaaltemple" -> Just VaalTemple
  "vault" -> Just Vault
  "vaultsofatziri" -> Just VaultsOfAtziri
  "villa" -> Just Villa
  "vintakrssquare" -> Just VintakrsSquare
  "volcano" -> Just Volcano
  "wasteland" -> Just Wasteland
  "wastepool" -> Just WastePool
  "waterways" -> Just Waterways
  "whakawairuatuahu" -> Just WhakawairuaTuahu
  "wharf" -> Just Wharf
  _ -> Nothing

mapFromName :: Name -> Map
mapFromName = \case
  Academy -> academy
  AcidCaverns -> acidCaverns
  ActonsNightmare -> actonsNightmare
  Alleyways -> alleyways
  AncientCity -> ancientCity
  ArachnidNest -> arachnidNest
  ArachnidTomb -> arachnidTomb
  Arcade -> arcade
  Arena -> arena
  AridLake -> aridLake
  Armoury -> armoury
  Arsenal -> arsenal
  AshenWood -> ashenWood
  Atoll -> atoll
  Barrows -> barrows
  Basilica -> basilica
  Bazaar -> bazaar
  Beach -> beach
  Belfry -> belfry
  Bog -> bog
  BoneCrypt -> boneCrypt
  BurialChambers -> burialChambers
  CaerBlaidd -> caerBlaidd
  Cage -> cage
  Caldera -> caldera
  Canyon -> canyon
  Carcass -> carcass
  CastleRuins -> castleRuins
  Cells -> cells
  Cemetery -> cemetery
  Channel -> channel
  Chateau -> chateau
  CitySquare -> citySquare
  Colonnade -> colonnade
  Colosseum -> colosseum
  Conservatory -> conservatory
  CoralRuins -> coralRuins
  Core -> core
  Courthouse -> courthouse
  Courtyard -> courtyard
  Coves -> coves
  CowardsTrial -> cowardsTrial
  Crater -> crater
  CrimsonTemple -> crimsonTemple
  CrystalOre -> crystalOre
  CursedCrypt -> cursedCrypt
  DarkForest -> darkForest
  DeathAndTaxes -> deathAndTaxes
  DefiledCathedral -> defiledCathedral
  Desert -> desert
  DesertSpring -> desertSpring
  Dig -> dig
  DoryanisMachinarium -> doryanisMachinarium
  Dunes -> dunes
  Dungeon -> dungeon
  Estuary -> estuary
  Excavation -> excavation
  Factory -> factory
  Fields -> fields
  FloodedMine -> floodedMine
  FungalHollow -> fungalHollow
  Garden -> garden
  Geode -> geode
  Ghetto -> ghetto
  Glacier -> glacier
  Graveyard -> graveyard
  Grotto -> grotto
  HallOfGrandmasters -> hallOfGrandmasters
  HallowedGround -> hallowedGround
  HauntedMansion -> hauntedMansion
  Iceberg -> iceberg
  InfestedValley -> infestedValley
  IvoryTemple -> ivoryTemple
  JungleValley -> jungleValley
  Laboratory -> laboratory
  Lair -> lair
  LavaChambers -> lavaChambers
  LavaLake -> lavaLake
  Leyline -> leyline
  Lighthouse -> lighthouse
  Lookout -> lookout
  MaelstromOfChaos -> maelstromOfChaos
  Malformation -> malformation
  MaoKun -> maoKun
  Marshes -> marshes
  Mausoleum -> mausoleum
  Maze -> maze
  Mesa -> mesa
  MineralPools -> mineralPools
  MoonTemple -> moonTemple
  MudGeyser -> mudGeyser
  Museum -> museum
  Necropolis -> necropolis
  ObasCursedTrove -> obasCursedTrove
  OlmecsSanctum -> olmecsSanctum
  Orchard -> orchard
  OvergrownRuin -> overgrownRuin
  OvergrownShrine -> overgrownShrine
  Palace -> palace
  Park -> park
  Pen -> pen
  Peninsula -> peninsula
  PerandusManor -> perandusManor
  Phantasmagoria -> phantasmagoria
  Pier -> pier
  PillarsOfArun -> pillarsOfArun
  Pit -> pit
  Plateau -> plateau
  Plaza -> plaza
  PoorjoysAsylum -> poorjoysAsylum
  Port -> port
  Precinct -> precinct
  PrimordialBlocks -> primordialBlocks
  PrimordialPool -> primordialPool
  Promenade -> promenade
  PutridCloister -> putridCloister
  RaceCourse -> raceCourse
  Ramparts -> ramparts
  Reef -> reef
  RelicChambers -> relicChambers
  Residence -> residence
  Scriptorium -> scriptorium
  Sepulchre -> sepulchre
  Shipyard -> shipyard
  Shore -> shore
  Shrine -> shrine
  Siege -> siege
  SpiderForest -> spiderForest
  SpiderLair -> spiderLair
  Strand -> strand
  SulphurVents -> sulphurVents
  Summit -> summit
  SunkenCity -> sunkenCity
  Temple -> temple
  Terrace -> terrace
  Thicket -> thicket
  Tower -> tower
  ToxicSewer -> toxicSewer
  TropicalIsland -> tropicalIsland
  TwilightTemple -> twilightTemple
  UndergroundRiver -> undergroundRiver
  UndergroundSea -> undergroundSea
  VaalPyramid -> vaalPyramid
  VaalTemple -> vaalTemple
  Vault -> vault
  VaultsOfAtziri -> vaultsOfAtziri
  Villa -> villa
  VintakrsSquare -> vintakrsSquare
  Volcano -> volcano
  WastePool -> wastePool
  Wasteland -> wasteland
  Waterways -> waterways
  WhakawairuaTuahu -> whakawairuaTuahu
  Wharf -> wharf

-------------------------------------------------------------------------------
-- Prefix Matching Data Below
-------------------------------------------------------------------------------
mapNames :: [Text]
mapNames = [
  "academy"
  , "acidcaverns"
  , "actonsnightmare"
  , "alleyways"
  , "ancientcity"
  , "arachnidnest"
  , "arachnidtomb"
  , "arcade"
  , "arena"
  , "aridlake"
  , "armoury"
  , "arsenal"
  , "ashenwood"
  , "atoll"
  , "barrows"
  , "basilica"
  , "bazaar"
  , "beach"
  , "belfry"
  , "bog"
  , "bonecrypt"
  , "burialchambers"
  , "caerblaidd"
  , "cage"
  , "caldera"
  , "canyon"
  , "carcass"
  , "castleruins"
  , "cells"
  , "cemetery"
  , "channel"
  , "chateau"
  , "citysquare"
  , "colonnade"
  , "colosseum"
  , "conservatory"
  , "coralruins"
  , "core"
  , "courthouse"
  , "courtyard"
  , "coves"
  , "cowardstrial"
  , "crater"
  , "crimsontemple"
  , "crystalore"
  , "cursedcrypt"
  , "darkforest"
  , "deathandtaxes"
  , "defiledcathedral"
  , "desert"
  , "desertspring"
  , "dig"
  , "doryanismachinarium"
  , "dunes"
  , "dungeon"
  , "estuary"
  , "excavation"
  , "factory"
  , "fields"
  , "floodedmine"
  , "fungalhollow"
  , "garden"
  , "geode"
  , "ghetto"
  , "glacier"
  , "graveyard"
  , "grotto"
  , "hallofgrandmasters"
  , "hallowedground"
  , "hauntedmansion"
  , "iceberg"
  , "infestedvalley"
  , "ivorytemple"
  , "junglevalley"
  , "laboratory"
  , "lair"
  , "lavachambers"
  , "lavalake"
  , "leyline"
  , "lighthouse"
  , "lookout"
  , "maelstromofchaos"
  , "malformation"
  , "maokun"
  , "marshes"
  , "mausoleum"
  , "maze"
  , "mesa"
  , "mineralpools"
  , "moontemple"
  , "mudgeyser"
  , "museum"
  , "necropolis"
  , "obascursedtrove"
  , "olmecssanctum"
  , "orchard"
  , "overgrownruin"
  , "overgrownshrine"
  , "palace"
  , "park"
  , "pen"
  , "peninsula"
  , "perandusmanor"
  , "phantasmagoria"
  , "pier"
  , "pillarsofarun"
  , "pit"
  , "plateau"
  , "plaza"
  , "poorjoysasylum"
  , "port"
  , "precinct"
  , "primordialblocks"
  , "primordialpool"
  , "promenade"
  , "putridcloister"
  , "racecourse"
  , "ramparts"
  , "reef"
  , "relicchambers"
  , "residence"
  , "scriptorium"
  , "sepulchre"
  , "shipyard"
  , "shore"
  , "shrine"
  , "siege"
  , "spiderforest"
  , "spiderlair"
  , "strand"
  , "sulphurvents"
  , "summit"
  , "sunkencity"
  , "temple"
  , "terrace"
  , "thicket"
  , "tower"
  , "toxicsewer"
  , "tropicalisland"
  , "twilighttemple"
  , "undergroundriver"
  , "undergroundsea"
  , "vaalpyramid"
  , "vaaltemple"
  , "vault"
  , "vaultsofatziri"
  , "villa"
  , "vintakrssquare"
  , "volcano"
  , "wasteland"
  , "wastepool"
  , "waterways"
  , "whakawairuatuahu"
  , "wharf"
  ]
