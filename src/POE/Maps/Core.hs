module POE.Maps.Core (
  Region(..)
  , Tier(..)
  , nextTier
  , prevTier
  , Name(..)
  , Map(..)
  , mapName
  , mapRegion
  , mapTiers
  , Atlas
  , mkAtlas
  , CompletedMaps(..)
) where

import YNotPrelude hiding (Map)

import qualified Data.Map as M

data Region
  = HaewarkHamlet
  | TirnsEnd
  | LexProxima
  | LexEjoris
  | NewVastir
  | GlennachCairns
  | ValdosRest
  | LiraArthain
  deriving (Show, Eq, Ord)

data Tier
  = T1
  | T2
  | T3
  | T4
  | T5
  | T6
  | T7
  | T8
  | T9
  | T10
  | T11
  | T12
  | T13
  | T14
  | T15
  | T16
  deriving (Show, Eq, Ord)

nextTier :: Tier -> Tier
nextTier = \case
  T1 -> T2
  T2 -> T3
  T3 -> T4
  T4 -> T5
  T5 -> T6
  T6 -> T7
  T7 -> T8
  T8 -> T9
  T9 -> T10
  T10 -> T11
  T11 -> T12
  T12 -> T13
  T13 -> T14
  T14 -> T15
  T15 -> T16
  T16 -> T16

prevTier :: Tier -> Tier
prevTier = \case
  T1 -> T1
  T2 -> T1
  T3 -> T2
  T4 -> T3
  T5 -> T4
  T6 -> T5
  T7 -> T6
  T8 -> T7
  T9 -> T8
  T10 -> T9
  T11 -> T10
  T12 -> T11
  T13 -> T12
  T14 -> T13
  T15 -> T14
  T16 -> T15

data Name
  -- Haewark Hamlet (1, Outer Top-left)
  = Leyline
  | VaalPyramid
  | VaultsOfAtziri
  | Conservatory
  | Academy
  | Phantasmagoria
  | Chateau
  | PerandusManor
  | CastleRuins
  -- 1 gem
  | Precinct
  -- 2 gems
  | Dig
  -- 3 gems
  | Malformation
  | Shrine
  -- 4 gems
  | Grotto
  | Terrace

  -- Tirn's End (2, Inner Top-left)
  | Wharf
  | Strand
  | WhakawairuaTuahu
  | Alleyways
  | Laboratory
  | CursedCrypt
  | CowardsTrial
  | Museum
  | PutridCloister
  | Temple
  | PoorjoysAsylum
  | PrimordialPool
  | Waterways
  -- 1 gem
  | TropicalIsland
  | Atoll
  | MaelstromOfChaos
  -- 2 gems
  | Iceberg
  -- 3 gems
  | Ghetto
  | CitySquare
  -- 4 gems
  | Necropolis
  | DeathAndTaxes
  | SpiderForest

  -- Lex Proxima (3, Inner Top-right)
  | JungleValley
  | Orchard
  | AshenWood
  | HauntedMansion
  | Mausoleum
  | UndergroundSea
  | ObasCursedTrove
  | Fields
  | Desert
  | Bazaar
  | Bog
  | Arsenal
  | Dunes
  | PillarsOfArun
  | Volcano
  | Belfry
  | Cemetery
  | HallowedGround
  | CrystalOre
  -- 1 gem
  | AridLake
  | Port
  -- 2 gems
  | Maze
  | DoryanisMachinarium
  | Residence
  -- 3 gems
  | Plateau
  | UndergroundRiver
  | CaerBlaidd
  -- 4 gems
  | IvoryTemple
  | CoralRuins

  -- Lex Ejoris (4, Outer Top-right)
  | Arena
  | Barrows
  | Pit
  | Thicket
  | BoneCrypt
  | OlmecsSanctum
  | Caldera
  | Reef
  | Canyon
  -- 1 gem
  | Lair
  | Shipyard
  -- 2 gems
  | Core
  -- 3 gems
  | CrimsonTemple
  | ArachnidNest
  -- 4 gems
  | Carcass
  | LavaLake

  -- NewVastir (5, Outer Bottom-left)
  | Arcade
  | Cage
  | MoonTemple
  | TwilightTemple
  | WastePool
  | Park
  | Courtyard
  | VintakrsSquare
  | Basilica
  -- 1 gem
  | Courthouse
  -- 2 gems
  | PrimordialBlocks
  -- 3 gems
  | SunkenCity
  -- 4 gems
  | Summit
  | AcidCaverns

  -- GlennachCairns (6, Inner Bottom-left)
  | Beach
  | SulphurVents
  | Ramparts
  | Glacier
  | Armoury
  | FungalHollow
  | Pen
  | Vault
  | RaceCourse
  | Coves
  | LavaChambers
  | Shore
  | MaoKun
  | Colonnade
  | Plaza
  -- 1 gem
  | Marshes
  | Excavation
  -- 2 gems
  | Cells
  | FloodedMine
  -- 3 gems
  | Channel
  | Dungeon
  -- 4 gems
  | InfestedValley
  | SpiderLair

  -- Valdo's Rest (7, Inner Bottom)
  | Pier
  | Villa
  | Peninsula
  | Lookout
  | Graveyard
  | OvergrownRuin
  | MudGeyser
  | Mesa
  | Wasteland
  -- 1 gem
  | Scriptorium
  | BurialChambers
  -- 2 gems
  | Factory
  | Lighthouse
  -- 3 gems
  | OvergrownShrine
  | Crater
  | ActonsNightmare
  -- 4 gems
  | VaalTemple
  | MineralPools
  | Promenade
  | HallOfGrandmasters

  -- Lira Arthain (8, Outer Bottom-right)
  | ToxicSewer
  | AncientCity
  | RelicChambers
  | DefiledCathedral
  | ArachnidTomb
  | Estuary
  | Geode
  | Tower
  -- 1 gem
  | Siege
  | Garden
  -- 2 gems
  | Sepulchre
  | Colosseum
  -- 3 gems
  | DarkForest
  -- 4 gems
  | Palace
  | DesertSpring
  deriving (Show, Eq, Ord)

data Map
  = Map Name [Tier] Region
  deriving (Show, Eq, Ord)


mapName :: Map -> Name
mapName (Map n _ _) = n

mapRegion :: Map -> Region
mapRegion (Map _ _ r) = r

mapTiers :: Map -> [Tier]
mapTiers (Map _ ts _) = ts

type Atlas = M.Map Name [Map]

mkAtlas :: [(Name, [Map])] -> Atlas
mkAtlas = M.fromList

newtype CompletedMaps = CompletedMaps [Map]
