# Path of Exile Mapper Tools

| poe-mapper-tools | 0.1.0.0                           |
| ---------------- | --------------------------------- |
| Maintainer       | Allele Dev (allele.dev@gmail.com) |
| Copyright        | 2019 Allele Dev                   |
| License          | BSD3                              |

## Overview

Just a small set of tools to work with [Path of Exile](https://www.pathofexile.com/)'s 3.9 Atlas.

## Examples

WIP

### Library Examples

* Viewing all T1 maps

```haskell
> t1s maps
[ Map Wharf [T1, T4, T8, T10, T14] TirnsEnd
, Map JungleValley [T1, T4, T8, T10, T14] LexProxima
, Map Beach [T1, T4, T8, T10, T14] GlennachCairns
, Map Pier [T1, T4, T8, T10, T14] ValdosRest
]
```

* Viewing all maps in the New Vastir region

```haskell
> inNewVastir maps
[ Map Arcade [T3, T6, T10, T11, T14] NewVastir
, Map Cage [T3, T7, T10, T12, T14] NewVastir
, Map MoonTemple [T3, T7, T10, T12, T14] NewVastir
, Map WastePool [T3, T8, T11, T12, T15] NewVastir
, Map Park [T4, T8, T11, T12, T15] NewVastir
, Map Courtyard [T4, T8, T11, T13, T15] NewVastir
, Map VintakrsSquare [T4, T8, T11, T13, T15] NewVastir
, Map Basilica [T5, T9, T12, T14, T16] NewVastir
, Map TwilightTemple [T3, T7, T10, T12, T14] NewVastir
, Map PrimordialBlocks [T12, T14, T16] NewVastir
, Map SunkenCity [T14, T16] NewVastir
, Map Summit [T16] NewVastir
, Map AcidCaverns [T15] NewVastir
]
```

* Viewing all maps in New Vastir that unlock when exactly two gems are
  socketed

```haskell
> (inNewVastir . unlockedAt2Gems) maps
[ Map PrimordialBlocks [T12, T14, T16] NewVastir
]
```

* Viewing all maps in Lira Arthain that are unlocked by the time one
  gem is socketed

```haskell
> (inLiraArthain . unlockedWithin1Gem) maps
[ Map ToxicSewer [T3, T7, T10, T12, T15] LiraArthain
, Map AncientCity [T3, T6, T10, T12, T14] LiraArthain
, Map RelicChambers [T3, T7, T10, T11, T14] LiraArthain
, Map DefiledCathedral [T4, T7, T10, T12, T14] LiraArthain
, Map ArachnidTomb [T4, T8, T11, T13, T15] LiraArthain
, Map Estuary [T4, T7, T10, T12, T15] LiraArthain
, Map Geode [T4, T8, T11, T13, T15] LiraArthain
, Map Tower [T5, T9, T12, T14, T16] LiraArthain
, Map Siege [T8, T11, T13, T15] LiraArthain
, Map Garden [T9, T12, T13, T16] LiraArthain
]
```

* Viewing all maps that can drop from a T3 Volcano, assuming certain
  other maps have been completed

```haskell
> fmap (\(Map n _ _) -> n) $ canDropFrom Volcano T3 atlas (CompletedMaps [carcass, pier, arsenal, temple, overgrownRuin, tropicalIsland, crimsonTemple])
[Temple,TropicalIsland,UndergroundSea,Arsenal,CoralRuins,Pit,Thicket,BoneCrypt,Pier,OvergrownRuin]
```

## Contributing

Contributions are welcome! Documentation, examples, code, and
feedback - they all help.

Be sure to review the included code of conduct. This project adheres
to the [Contributor's Covenant](http://contributor-covenant.org/). By
participating in this project you agree to abide by its terms.

This project currently has no funding, so it is maintained strictly on
the basis of its use to me. No guarantees are made about attention to
issues or contributions, or timeliness thereof.

## Developer Setup

The easiest way to start contributing is to install
[stack](https://github.com/commercialhaskell/stack). stack can install
GHC/Haskell for you, and automates common developer tasks.

The key commands are:

* `stack setup`: install GHC
* `stack build`: build the project
* `stack clean`: clean build artifacts
* `stack haddock`: builds documentation
* `stack test`: run all tests
* `stack bench`: run all benchmarks
* `stack ghci`: start a REPL instance

## Licensing

This project is distributed under the BSD3 license. See the included
[LICENSE](./LICENSE) file for more details.
